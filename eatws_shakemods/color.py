from impactutils.colors.cpalette import ColorPalette
from matplotlib.colors import LinearSegmentedColormap
import numpy as np

DEFAULT_NCOLORS = 256
MMI = {'z0': np.arange(0, 10),
       'z1': np.arange(1, 11),
       'rgba0': [(191, 204, 255, 0),
                (191, 204, 255, 0),
                (191, 204, 255, 255),
                (160, 230, 255, 255),
                (128, 255, 255, 255),
                (122, 255, 147, 255),
                (255, 255, 0, 255),
                (255, 200, 0, 255),
                (255, 145, 0, 255),
                (255, 0, 0, 255)],
       'rgba1': [(191, 204, 255, 0),
                (191, 204, 255, 255),
                (160, 230, 255, 255),
                (128, 255, 255, 255),
                (122, 255, 147, 255),
                (255, 255, 0, 255),
                (255, 200, 0, 255),
                (255, 145, 0, 255),
                (255, 0, 0, 255),
                (200, 0, 0, 255)],
       'nan_color': (0, 0, 0, 0),
       'resolution': 0.1}

def MMIPalette():
    return TransparentPalette(name='MMI', **MMI)

class TransparentPalette(ColorPalette):
    def __init__(self, name, z0, z1, rgba0, rgba1, resolution=None, nan_color=0, is_log=False):
        """Construct a DataColorMap from input Z values and RGBA specs.

        Args:
            name: Name of colormap.
            z0: Sequence of z0 values.
            z1: Sequence of z1 values.
            rgba0: Sequence of RGB quadruplets (values between 0-255).
            rgba1: Sequence of RGB quadruplets (values between 0-255).
            resolution: Desired Resolution of the data values in data units.
                For example, the preset population color map has a resolution
                of 1.0, meaning that we want to be able to distinguish between
                color values associated with a difference of 1 person. This
                sets the number of colors to be:
                    `max(256,int((max(z1)-min(z0))/resolution))`
            nan_color: Either 0 or RGBA quadruplet (A is for Alpha, where 0 is
                transparent, and 255 is opaque.)
        """
        # validate that lengths are all identical
        if len(z0) != len(z1) != len(rgba0) != len(rgba1):
            raise Exception('Lengths of input sequences to ColorPalette() '
                            'must be identical.')
        self._is_log = is_log
        z0 = np.array(z0)
        z1 = np.array(z1)
        self._vmin = z0.min()
        self._vmax = z1.max()
        if isinstance(nan_color, int):
            nan_color = [nan_color] * 4
        self.nan_color = np.array(nan_color) / 255.0

        # Change the z values to be between 0 and 1
        adj_z0 = (z0 - self._vmin) / (self._vmax - self._vmin)
        # should this be z0 - vmin?
        adj_z1 = (z1 - self._vmin) / (self._vmax - self._vmin)

        # loop over the sequences, and construct a dictionary of red, green,
        # blue tuples
        B = -.999 * 255
        # this will mark the y0 value in the first row (isn't used)
        E = .999 * 255
        # this will mark the y1 value in the last row (isn't used)

        # if we add dummy rows to our rgba sequences, we can do one simple loop
        # through.
        rgba0_t = rgba0.copy()
        rgba1_t = rgba1.copy()
        # append a dummy row to the end of rgba0
        rgba0_t.append((E, E, E, E))
        # prepend a dummy row to the beginning of rgba1
        rgba1_t.insert(0, (B, B, B, B))
        # Make the column of x values have the same length as the rgba sequences
        x = np.append(adj_z0, adj_z1[-1])

        cdict = {'red': [],
                 'green': [],
                 'blue': [],
                 'alpha': [],
                 }

        for i in range(0, len(x)):
            red0 = rgba1_t[i][0] / 255.0
            red1 = rgba0_t[i][0] / 255.0
            green0 = rgba1_t[i][1] / 255.0
            green1 = rgba0_t[i][1] / 255.0
            blue0 = rgba1_t[i][2] / 255.0
            blue1 = rgba0_t[i][2] / 255.0
            alpha0 = rgba1_t[i][3] / 255.0
            alpha1 = rgba0_t[i][3] / 255.0
            cdict['red'].append((x[i], red0, red1))
            cdict['green'].append((x[i], green0, green1))
            cdict['blue'].append((x[i], blue0, blue1))
            cdict['alpha'].append((x[i], alpha0, alpha1))

        self._cdict = cdict.copy()
        # choose the number of colors to store the colormap
        # if we choose too low, then there may not be enough colors to
        # accurately capture the resolution of our data.
        # this isn't perfect
        numcolors = DEFAULT_NCOLORS
        if resolution is not None:
            ncolors_tmp = np.ceil((self._vmax - self._vmin) / resolution)
            numcolors = max(DEFAULT_NCOLORS, ncolors_tmp)

        self._cmap = LinearSegmentedColormap(name, cdict, N=numcolors)
        self._cmap.set_bad(self.nan_color)
