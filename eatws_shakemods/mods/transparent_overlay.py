# stdlib imports
import os.path
import zipfile
import shutil
import re

# third party imports
from shapely.geometry import shape
import fiona
from impactutils.io.smcontainers import ShakeMapOutputContainer
from PIL import Image
import configobj
import numpy as np
from mapio.geodict import GeoDict
import cartopy.io.shapereader as shpreader

# USGS imports
from shakemap.coremods.base import CoreModule, Contents
from shakemap.utils.config import (get_config_paths,
                                   get_configspec,
                                   get_custom_validator,
                                   config_error)
from mapio.grid2d import Grid2D

# GA imports
from ..color import MMIPalette

class TransparentOverlayModule(CoreModule):
    """
    transparent_overlay -- Generate an *actually* transparent MMI overlay PNG
    """

    command_name = 'transparent_overlay'
    targets = [r'products/transparent_overlay.png']
    dependencies = [('products/shake_result.hdf', True)]


    def __init__(self, eventid):
        super().__init__(eventid)
        self.contents = Contents('Transparent MMI Overlay PNG', 'transparent_overlay', eventid)

    def execute(self):
        install_path, data_path = get_config_paths()
        datadir = os.path.join(data_path, self._eventid, 'current', 'products')
        if not os.path.isdir(datadir):
            raise NotADirectoryError('%s is not a valid directory.' % datadir)
        datafile = os.path.join(datadir, 'shake_result.hdf')
        if not os.path.isfile(datafile):
            raise FileNotFoundError('%s does not exist.' % datafile)

        # Open the ShakeMapOutputContainer and extract the data
        container = ShakeMapOutputContainer.load(datafile)

        if container.getDataType() != 'grid':
            raise NotImplementedError('transparent_overlay module can only use '
                                      'gridded data, not sets of points')

        # find the low res ocean vector dataset
        product_config_file = os.path.join(
            install_path, 'config', 'products.conf')
        spec_file = get_configspec('products')
        validator = get_custom_validator()
        pconfig = configobj.ConfigObj(product_config_file,
                                      configspec=spec_file)
        results = pconfig.validate(validator)
        if not isinstance(results, bool) or not results:
            config_error(pconfig, results)

        # use highres oceans
        oceanfile = shpreader.natural_earth(category='physical',
                                            name='ocean',
                                            resolution='10m')

        # call create_kmz function
        create_overlay_image(container, oceanfile, datadir)

        self.contents.addFile('transparent_overlay',
                              'Transparent MMI Overlay PNG',
                              'Transparent MMI Overlay PNG.',
                              'transparent_overlay.png',
                              'image/png')

        container.close()

def create_overlay_image(container, oceanfile, datadir):
    filename = os.path.join(datadir, 'transparent_overlay.png')

    # extract the intensity data from the container
    comp = container.getComponents('MMI')[0]
    imtdict = container.getIMTGrids('MMI', comp)
    mmigrid = imtdict['mean']
    gd = GeoDict(imtdict['mean_metadata'])
    imtdata = mmigrid.copy()
    rows, cols = imtdata.shape

    # get the intensity colormap
    palette = MMIPalette()

    # replace missing data with MMI 0
    imtdata[np.isnan(imtdata)] = 0

    # map intensity values into
    # RGBA array
    rgba = palette.getDataColor(imtdata, color_format='array')

    # mask off the areas covered by ocean
    bbox = (gd.xmin, gd.ymin, gd.xmax, gd.ymax)
    with fiona.open(oceanfile) as c:
        tshapes = list(c.items(bbox=bbox))
        shapes = []
        for tshp in tshapes:
            shapes.append(shape(tshp[1]['geometry']))
        if len(shapes):
            oceangrid = Grid2D.rasterizeFromGeometry(shapes, gd, fillValue=0.0)
            rgba[oceangrid.getData() == 1] = 0

    # save rgba image as png
    img = Image.fromarray(rgba)
    img.save(filename)
    return gd
