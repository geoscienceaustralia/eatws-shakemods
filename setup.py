from distutils.core import setup

setup(name='eatws_shakemods',
      version='0.3',
      description='EATWS/NEAC shakemap plugins',
      author='Anthony Carapetis',
      author_email='anthony.carapetis@ga.gov.au',
      packages=['eatws_shakemods', 'eatws_shakemods.mods'],
     )
